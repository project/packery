<?php

namespace Drupal\packery_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\packery\Entity\PackeryGroup;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Defines a style plugin that renders a Packery grid layout.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "packery_views",
 *   title = @Translation("Packery"),
 *   help = @Translation("Displays view output in Packery grid."),
 *   theme = "packery_view_grid",
 *   display_types = {"normal"}
 * )
 */
class PackeryViewsStyle extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['settings'] = ['default' => 'default_group'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['settings'] = [
      '#title' => $this->t('Settings group'),
      '#type' => 'select',
      '#options' => PackeryGroup::getPackerySettingsList(),
      '#description' => $this->t('The settings group to apply.'),
      '#default_value' => $this->options['settings'] ? $this->options['settings'] : 'default_group',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRowClass($row_index) {
    $classes = parent::getRowClass($row_index);
    return "$classes item-$row_index";
  }

}
