<?php

namespace Drupal\packery\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines a class to list settings groups.
 *
 * @see \Drupal\user\Entity\Group
 */
class PackeryController extends ControllerBase {

  /**
   * Settings group list.
   */
  public function groupOverview() {
    return \Drupal::formBuilder()->getForm('Drupal\packery\Form\PackeryGroupDisplayForm');
  }

}
