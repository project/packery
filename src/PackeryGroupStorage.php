<?php

namespace Drupal\packery;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Storage for group entities.
 */
class PackeryGroupStorage extends ConfigEntityStorage {}
