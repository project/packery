<?php

namespace Drupal\packery\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Group entity.
 *
 * The group entity stores information about a settings group.
 *
 * @ConfigEntityType(
 *   id = "packery_group",
 *   label = @Translation("Packery Group"),
 *   module = "packery",
 *   config_prefix = "group",
 *   admin_permission = "administer packery settings",
 *   handlers = {
 *     "storage" = "Drupal\packery\PackeryGroupStorage",
 *     "form" = {
 *       "default" = "Drupal\packery\Form\PackeryGroupForm",
 *       "delete" = "Drupal\packery\Form\PackeryGroupDeleteForm"
 *     },
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/group/manage/{packery_group}",
 *     "delete-form" = "/admin/config/group/manage/{packery_group}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "settings"
 *   }
 * )
 */
class PackeryGroup extends ConfigEntityBase implements PackeryGroupInterface {

  /**
   * The group machine name.
   *
   * @var string
   */
  protected $id;

  /**
   * The group human readable name.
   *
   * @var string
   */
  protected $label;

  /**
   * The array of settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * Returns Packery settings group to initialize.
   *
   * @param string $id
   *   The Packery Group id.
   *
   * @return array
   *   Packery settings for the Packery script.
   */
  public static function getPackerySettings($id) {

    /** @var \Drupal\packery\Entity\PackeryGroup $group */
    $group = static::load($id);

    if ($group) {
      $settings = $group->getSettings();

      $group = [
        'id' => $group->id(),
        'label' => $group->label(),
        'settings' => [
          'itemSelector' => $settings['item_selector'],
          'columnWidth' => $settings['column_width'],
          'rowHeight' => $settings['row_height'],
          'gutter' => $settings['gutter'],
          'percentPosition' => (bool) $settings['percent_position'],
          'stamp' => $settings['stamp'],
          'isHorizontal' => (bool) $settings['is_horizontal'],
          'isOriginLeft' => (bool) $settings['is_origin_left'],
          'isOriginTop' => (bool) $settings['is_origin_top'],
          'transitionDuration' => $settings['transition_duration'],
          'containerStyle' => $settings['container_style'],
          'isResizeBound' => (bool) $settings['is_resize_bound'],
          'isInitLayout' => (bool) $settings['is_init_layout'],
        ],
        'extend' => [
          'images_loaded' => $settings['images_loaded'],
        ],
      ];

      // Allow alter on group settings.
      \Drupal::moduleHandler()->alter('settings', $group);

      return $group;
    }

    return [];
  }

  /**
   * Returns a list of all available settings groups.
   */
  public static function getPackerySettingsList() {
    $options = [];

    foreach (static::loadMultiple() as $group) {
      $options[$group->id()] = $group->label();
    }

    return $options;
  }

}
