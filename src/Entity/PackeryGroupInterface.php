<?php

namespace Drupal\packery\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for Packery settings groups.
 */
interface PackeryGroupInterface extends ConfigEntityInterface {

  /**
   * Get the Packery Group settings.
   *
   * @return array
   *   The setting values.
   */
  public function getSettings();

}
