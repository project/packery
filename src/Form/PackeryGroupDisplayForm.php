<?php

namespace Drupal\packery\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\packery\Entity\PackeryGroup;

/**
 * Packery Group settings form.
 */
class PackeryGroupDisplayForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'packery_group_display_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $table = [
      '#type' => 'table',
      '#header' => ['Name', 'Description', 'Value'],
    ];

    $form['settings'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Settings groups'),
      '#parents' => ['settings'],
    ];

    // Retrieve config definitions.
    $definitions = \Drupal::service('config.typed')->getDefinition('packery.group.default');
    $settings = $definitions['mapping']['settings']['mapping'];

    // Retrieve config entity values.
    /** @var \Drupal\packery\Entity\PackeryGroup[] $groups */
    $groups = PackeryGroup::loadMultiple();
    foreach ($groups as $group) {
      $form[$group->id()] = [
        '#type' => 'details',
        '#title' => $this->t('@title', ['@title' => $group->label()]),
        '#group' => 'settings',
      ];

      $form[$group->id()]['table'] = $table;
      foreach ($group->getSettings() as $name => $value) {
        $form[$group->id()]['table']['#rows'][] = [
          'name' => $this->t('@label', ['@label' => $settings[$name]['label']]),
          'description' => $this->t('@text', ['@text' => $settings[$name]['text']]),
          'value' => $value,
        ];
      }

      $form[$group->id()]['actions'] = [
        '#type' => 'container',
      ];
      $form[$group->id()]['actions']['edit'] = [
        '#type' => 'submit',
        '#name' => $group->id(),
        '#value' => 'Edit',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $group = $form_state->getTriggeringElement();
    $form_state->setRedirect('entity.packery_group.edit_form', ['packery_group' => $group['#name']]);
  }

}
